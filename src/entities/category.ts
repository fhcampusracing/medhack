export enum Category {
  INBOUND = 'standard',
  TRAVEL = 'reise'
}

export function findCategory(value: string): Category | undefined {
  switch (value) {
    case Category.INBOUND.toString():
      return Category.INBOUND;
    case Category.TRAVEL.toString():
      return Category.TRAVEL;
    default:
      return undefined;
  }
}
