export class Profile {
  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }
  private _username: string;
}
