import {Category} from './category';

export class Vaccination {
  get nameIllness(): string {
    return this._nameIllness;
  }

  set nameIllness(value: string) {
    this._nameIllness = value;
  }

  get lastShot(): Date {
    return this._lastShot;
  }

  set lastShot(value: Date) {
    this._lastShot = value;
  }

  get ingredient(): string {
    return this._ingredient;
  }

  set ingredient(value: string) {
    this._ingredient = value;
  }

  get cycle(): number {
    return this._cycle;
  }

  set cycle(value: number) {
    this._cycle = value;
  }

  get minRecommendedAge(): number {
    return this._minRecommendedAge;
  }

  set minRecommendedAge(value: number) {
    this._minRecommendedAge = value;
  }

  get maxRecommendedAge(): number {
    return this._maxRecommendedAge;
  }

  set maxRecommendedAge(value: number) {
    this._maxRecommendedAge = value;
  }

  get category(): Category | undefined {
    return this._category;
  }

  set category(value: Category | undefined) {
    this._category = value;
  }

  get id(): string {
    return this._id;
  }

  set id(id: string) {
    if (id) {
      this._id = id;
    } else {
      this._id = '';
    }
  }

  // tslint:disable-next-line:variable-name
  private _nameIllness!: string;
  // tslint:disable-next-line:variable-name
  private _lastShot!: Date;
  // tslint:disable-next-line:variable-name
  private _ingredient!: string;
  // tslint:disable-next-line:variable-name
  private _cycle!: number;
  // tslint:disable-next-line:variable-name
  private _minRecommendedAge!: number;
  // tslint:disable-next-line:variable-name
  private _maxRecommendedAge!: number;
  // tslint:disable-next-line:variable-name
  private _category!: Category | undefined;
  // tslint:disable-next-line:variable-name
  private _id = '';
}
