import {Gender} from './gender';

export class Person {
  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get birthdate(): Date {
    return this._birthdate;
  }

  set birthdate(value: Date) {
    this._birthdate = value;
  }

  get gender(): Gender {
    return this._gender;
  }

  set gender(value: Gender) {
    this._gender = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }
  // @ts-ignore
  private _firstName: string;
  // @ts-ignore
  private _lastName: string;
  // @ts-ignore
  private _birthdate: Date;
  // @ts-ignore
  private _gender: Gender;
  // @ts-ignore
  private _email: string;
}
