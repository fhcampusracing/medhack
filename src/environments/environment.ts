// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB3_-Jn_6DPCH6xWv99N0q5A_uagurhBw0',
    authDomain: 'medhack-85ed4.firebaseapp.com',
    databaseURL: 'https://medhack-85ed4-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'medhack-85ed4',
    storageBucket: 'medhack-85ed4.appspot.com',
    messagingSenderId: '774531787928',
    appId: '1:774531787928:web:22ac7edd9ef0e9488a34ed',
    measurementId: 'G-NLK2KT0QHF'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
