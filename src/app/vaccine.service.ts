import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {Vaccination} from '../entities/vaccination';
import {findCategory} from '../entities/category';

@Injectable({
  providedIn: 'root'
})
export class VaccineService {
  constructor(private db: AngularFirestore) {
  }

  getVaccines(): Observable<any[]> {
    return this.db.collection('/vaccines').valueChanges({idField: 'propertyId'});
  }

  mapValueToVaccination(value: any): Vaccination {
    const vaccination = new Vaccination();
    vaccination.category = findCategory(value.category);
    vaccination.nameIllness = value.name;
    vaccination.cycle = value.cycleYears;
    vaccination.maxRecommendedAge = value.recommendedMaxAge;
    vaccination.minRecommendedAge = value.recommendedMinAge;
    vaccination.id = value.propertyId;
    return vaccination;
  }

  mapImpfungToVaccination(value: any): Vaccination {
    const vaccination = new Vaccination();
    vaccination.category = findCategory(value._category);
    vaccination.nameIllness = value._nameIllness;
    vaccination.cycle = value._cycle;
    vaccination.maxRecommendedAge = value._maxRecommendedAge;
    vaccination.minRecommendedAge = value._minRecommendedAge;
    vaccination.lastShot = value._lastShot;
    vaccination.id = value._id;
    return vaccination;
  }

  mapImpfungenToVaccination(values: []): Array<Vaccination> {
    const vaccines: Array<Vaccination> = [];
    for (const value of values) {
      vaccines.push(this.mapImpfungToVaccination(JSON.parse(value)));
    }
    return vaccines;
  }
}
