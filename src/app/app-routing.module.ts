import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListVaccinesComponent} from './list-vaccines/list-vaccines.component';
import {ProfileComponent} from './profile/profile.component';
import {ListPersonsComponent} from './list-persons/list-persons.component';
import {AddChildComponent} from './add-child/add-child.component';
import {BookAppointmentComponent} from './book-appointment/book-appointment.component';

const routes: Routes = [{path: '', redirectTo: '/persons', pathMatch: 'full'},
  {path: 'vaccines', component: ListVaccinesComponent},
  {path: 'add-child', component: AddChildComponent},
  {path: 'book-appointment', component: BookAppointmentComponent},
  {path: 'profile/:id', component: ProfileComponent},
  {path: 'persons', component: ListPersonsComponent},
  {path: '', redirectTo: '/vaccines', pathMatch: 'full'},
  {path: 'add-child', component: AddChildComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
