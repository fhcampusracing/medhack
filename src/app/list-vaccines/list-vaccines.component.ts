import {Component, OnInit} from '@angular/core';
import {VaccineService} from '../vaccine.service';
import {Vaccination} from '../../entities/vaccination';

@Component({
  selector: 'app-list-vaccines',
  templateUrl: './list-vaccines.component.html',
  styleUrls: ['./list-vaccines.component.css']
})
export class ListVaccinesComponent implements OnInit {

  displayedColumns: string[] = ['category', 'cycleYears', 'name', 'recommendedMaxAge', 'recommendedMinAge'];
  displayData = false;
  dataSource: Array<Vaccination> = [];

  constructor(private vaccineService: VaccineService) {
  }

  ngOnInit(): void {
    this.displayData = false;
    this.vaccineService.getVaccines().subscribe(values => {
        for (const value of values) {
          this.dataSource.push(this.vaccineService.mapValueToVaccination(value));
        }
        this.displayData = true;
      }
    );
  }
}
