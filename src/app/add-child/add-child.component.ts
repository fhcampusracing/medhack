import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AngularFirestore} from '@angular/fire/firestore';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-add-child',
  templateUrl: './add-child.component.html',
  styleUrls: ['./add-child.component.css']
})
export class AddChildComponent implements OnInit {

  addProfileForm = new FormGroup({
    firstname: new FormControl(),
    lastname: new FormControl(),
    birthdate: new FormControl()
  });

  parentId = 'gFE63e69o7nUlvnR4k2m';
  newDocId = '';
  parentDoc: any;

  constructor(
    private fb: FormBuilder,
    private db: AngularFirestore,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.addProfileForm = new FormGroup({
      firstname: new FormControl(),
      lastname: new FormControl(),
      birthdate: new FormControl()
    });

    this.createForm('', '' , '');

  }

  // tslint:disable-next-line:typedef
  createForm(firstname: string, lastname: string, birthdate: string){
    this.addProfileForm = this.fb.group({
      firstname: [firstname, Validators.required],
      lastname: [lastname, Validators.required],
      birthdate: [birthdate, Validators.required]
    });
  }

  addProfile(value: any): void {
    console.log(value);
    value.impfungen = [];
    value.email = '';
    value.children = [];
    this.db.collection('/persons').add(value).then(docRef => {
      console.log(docRef.id);
      this.newDocId = docRef.id;

      this.db.collection('/persons').doc(this.parentId).ref.get().then(doc => {
        this.parentDoc = doc.data();
        console.log('ParentDoc: ');
        console.log(this.parentDoc);
        console.log('ChildDoc Ref: ');
        console.log(docRef);
        this.parentDoc.children.push(docRef);

        this.db.collection('/persons').doc(this.parentId).update(this.parentDoc).then(_ => {
          this.toastr.success('Successfully added Profile!');
        });
      });

    });
  }

}
