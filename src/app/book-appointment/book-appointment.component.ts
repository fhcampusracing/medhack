import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {delay} from 'rxjs/operators';
import {Router} from '@angular/router';

interface Doctor {
  name: string;
  email: string;
}

@Component({
  selector: 'app-book-appointment',
  templateUrl: './book-appointment.component.html',
  styleUrls: ['./book-appointment.component.css']
})
export class BookAppointmentComponent implements OnInit {
  doctors: Doctor[] = [
    {name: 'Dr. Matthias Hauser', email: 'hauser@gmail.com'},
    {name: 'Dr. Claudia Fischer', email: 'fischer@gmail.com'},
    {name: 'Dr. Steven Granger', email: 'granger@gmail.com'}
  ];

  selectedDoctor = '';

  constructor(private toastr: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
  }

  selectDoctor(name: string): void {
    this.selectedDoctor = name;
  }

  bookAppointment(): void {
    this.toastr.success('Appointment suggestion was successfully sent to your doctor!');
    setTimeout(() =>
      {
        this.router.navigate(['/']);
      },
      5000);
  }

}
