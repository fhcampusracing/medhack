import {Component, OnInit} from '@angular/core';
import {Person} from '../../entities/person';
import {AngularFirestore} from '@angular/fire/firestore';
import {ActivatedRoute} from '@angular/router';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {VaccineService} from '../vaccine.service';
import {Vaccination} from '../../entities/vaccination';
import {PersonService} from '../person.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  date: any;
  person: any;
  allPossibleVaccines: Array<Vaccination> = [];
  displayData = false;
  id: string | null = '';
  private personVaccines = [];
  personVaccinesShow: Array<Vaccination> = [];
  fname: any;
  lname: any;

  childName = '';
  children: string[] = [];
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  constructor(private db: AngularFirestore,
              private route: ActivatedRoute,
              private vaccineService: VaccineService,
              private personService: PersonService) {
  }

  ngOnInit(): void {
    this.displayData = false;
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.personVaccines = [];
      this.personVaccinesShow = [];
      this.children = [];
      this.db.collection<Person>('/persons').doc(this.id).ref.get().then(
        doc => {
          this.person = doc.data();
          this.date = new FormControl(new Date(this.person.birthdate));
          this.emailFormControl.setValue(this.person.email);
          this.personVaccinesShow = this.vaccineService.mapImpfungenToVaccination(this.person.impfungen);
          this.children = this.person.children;
          this.fname = new FormControl(this.person.firstname);
          this.lname = new FormControl(this.person.lastname);
          for (const value of this.person.impfungen) {
            // @ts-ignore
            this.personVaccines.push(value);
          }

        }
      );
      this.getVaccines();
    }
  }

  private getVaccines(): void {
    this.allPossibleVaccines = [];
    this.vaccineService.getVaccines().subscribe(values => {
      for (const value of values) {
        this.allPossibleVaccines.push(this.vaccineService.mapValueToVaccination(value));
      }
      this.displayData = true;
    });
  }

  addVaccine(vaccine: Vaccination): void {
    if (this.id) {
      vaccine.lastShot = new Date();
      this.personVaccinesShow.push(vaccine);
      // @ts-ignore
      this.personVaccines.push(JSON.stringify(vaccine));
      // @ts-ignore
      this.personService.addVaccine(this.id, this.personVaccines).then();
    }
  }

  onSave(): void {
    if (this.id) {
      this.person.birthdate = this.date.value.toISOString();
      this.person.email = this.emailFormControl.value;
      this.person.firstName = this.fname.value;
      this.person.lastName = this.lname.value;
      this.personService.savePerson(this.id, this.person).then();
    }
  }

  addChild(): void {
    if (this.id) {
      this.children.push(this.childName);
      this.personService.addChild(this.id, this.children).then();
    }
  }
}
