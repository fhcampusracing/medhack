import {Component, OnInit} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-list-persons',
  templateUrl: './list-persons.component.html',
  styleUrls: ['./list-persons.component.css']
})
export class ListPersonsComponent implements OnInit {
  displayedColumns: string[] = ['birthdate', 'children', 'email', 'firstname', 'impfungen', 'lastname', 'Action'];
  dataSource = [];
  displayData = false;

  constructor(private db: AngularFirestore) {
  }

  ngOnInit(): void {
    this.displayData = false;
    this.db.collection('/persons').valueChanges({idField: 'propertyId'}).subscribe(values => {
      for (const value of values) {
        const person = {
          // @ts-ignore
          birthdate: new Date(value.birthdate),
          // @ts-ignore
          children: value.children.length,
          // @ts-ignore
          email: value.email,
          // @ts-ignore
          firstname: value.firstname,
          // @ts-ignore
          impfungen: value.impfungen.length,
          // @ts-ignore
          lastname: value.lastname,
          // @ts-ignore
          id: value.propertyId
        };

        // @ts-ignore
        this.dataSource.push(person);
      }
      this.displayData = true;
    });
  }

}
