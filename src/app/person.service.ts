import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Person} from '../entities/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  readonly COLLECTION_NAME = 'persons';

  constructor(private db: AngularFirestore) {
  }

  addVaccine(id: string, vaccines: []): Promise<void> {
    return this.db.collection(this.COLLECTION_NAME).doc(id).update({
      impfungen: vaccines
    });
  }

  savePerson(id: string, person: Person): Promise<void> {
    return this.db.collection(this.COLLECTION_NAME).doc(id).update({
      birthdate: person.birthdate,
      email: person.email,
      firstname: person.firstName,
      lastname: person.lastName,
      // gender: person.gender
    });
  }

  addChild(id: string, children: string[]): Promise<void> {
    return this.db.collection(this.COLLECTION_NAME).doc(id).update({
      children
    });
  }
}
